const express = require('express');
const router = express.Router();

const mysqlController = require('../controllers/mysql-controller.js');

/* GET home page. */
router.get('/', function(req, res){
	res.send(mysqlController.mysqlSelect());
});

module.exports = router;
