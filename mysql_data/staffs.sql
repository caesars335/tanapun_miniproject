SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `staffs` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `staffs` (`id`, `name`, `address`, `city`, `state`, `zip`) VALUES
(1, 'Bucky Roberts', '32 Hungerford Ave', 'Adams', 'NY', 13605),
(2,'Prayut Jan-O-Cha','Thailand','Bankok','the whole Bankok',10250),
(3,'Prawit Wongsuwan','Thailand','Bankok','the whole Bankok',10250);